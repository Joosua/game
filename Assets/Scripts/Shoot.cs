﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

public class Shoot : NetworkBehaviour {

    public int dmg = 1;
    public float cooldown = 3;
    public float cdtimer;

    public float firerate = 0;
    float timetofire = 0;

    public LayerMask whatToHit;

    public Transform Laser;

    [SerializeField]
    public RaycastHit2D hit;
    public Transform Shootpoint;

	// Use this for initialization
	void Awake () {

        Shootpoint = transform.FindChild("Shootpoint");

        if (Shootpoint == null)
        {
            Debug.Log("No 'shootpoint' set!");
        }
	}

    void Start()
    {
        
    }
	
	// Update is called once per frame
	void Update () 
    {
        CheckShooting();
        UpdateCooldown();
	}

    void CheckShooting()
    {
        if (!isLocalPlayer)
        {
            return;
        }

        if (Input.GetButtonDown ("Fire1") && cdtimer == 0)
        {
            Shooting();
            cdtimer = cooldown;
        }
        
    }

    void Shooting()
    {

        Vector2 ShootPointPos = new Vector2(Shootpoint.position.x, Shootpoint.position.y);
        Vector2 ShootDirection = (Vector2)transform.position - ShootPointPos;

        RaycastHit2D hit = Physics2D.Raycast(ShootPointPos, ShootDirection.normalized, -100, whatToHit);
        CmdEffect();
        

        Debug.DrawLine(ShootPointPos, ShootDirection.normalized * -100);
        Debug.Log(hit.transform.tag);

        if (hit.transform.tag == "Player")
        {
            string uIdentity = hit.transform.name;
            CmdTellServerWhoWasShot(uIdentity, dmg);
        }

        if (hit.transform.tag == "Untagged")
        {
            Debug.Log("Hit nothing");
        }
    }

    [Command]void CmdEffect()
    {     
        GameObject bullet = (GameObject)Instantiate(Laser.gameObject, Shootpoint.position, Shootpoint.rotation);
        NetworkServer.Spawn(bullet);
    }

    void UpdateCooldown()
    {
        if (cdtimer > 0)
        {
            cdtimer -= Time.deltaTime;
        }

        if (cdtimer < 0)
        {
            cdtimer = 0;
        }
    }

    [Command]
    void CmdTellServerWhoWasShot(string uniqueID, int dmg)
    {
        GameObject go = GameObject.Find(uniqueID);
        go.GetComponent<hp>().lowerHealth(dmg);
    }  
}
