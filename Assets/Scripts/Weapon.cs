﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Weapon : NetworkBehaviour {

    // Otetaan Tulipallon prefab käyttöön
    public Transform fireballtPrefab;

    public float shootRange = 1f;
    // Ampumisen cooldown
    private float cd;

	void Start () {
        // Alussa, ennen ampumista cooldown on 0
        cd = 0f;
	}
	
	void Update () {

        if (cd > 0)
        {
            cd -= Time.deltaTime;
        }
	}
    // Pelaajan ammuttua, jos ammuksen/lyönnin kohde on vihollinen, vihollinen ottaa vahinkoa (vahinko määritelty toisessa scriptissä)
    public void Attack(bool isEnemy)
    {
        if (CanAttack)
        {
            cd = shootRange;
            
            var shotTransform = Instantiate(fireballtPrefab) as Transform;
            // Tulipallon sijainti (lähtee ampujan kohdalta liikkeelle)
            shotTransform.position = transform.position;
            // Haetaan Shot-scripti käyttöön
            Shot shot = shotTransform.gameObject.GetComponent<Shot>();

            if (shot != null)
            {
                shot.isShot = isEnemy;
            }
            // Haetaan Move-scripti käyttöön
            Move move = shotTransform.gameObject.GetComponent<Move>();

            if (move != null)
            {
                move.direction = this.transform.right;
            }
        }
    }
    // Pelaaja voi ampua/lyödä, jos cooldown on 0
    public bool CanAttack
    {
        get
        {
            return cd <= 0f;
        }
    }
}
