﻿using UnityEngine;
using System.Collections;

public class Move : MonoBehaviour {

    public Vector2 speed = new Vector2(10 , 10);

    public Vector2 direction = new Vector2(-1, 0);

    public Vector2 move;

    Rigidbody2D rb;

	// Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

	void Update () {

        move = new Vector2(speed.x * direction.x , speed.y * direction.y);
	}
	
	// Update is called once per frame
	void FixedUpdate () {

        rb.velocity = move;
	}
}
