﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Health : NetworkBehaviour {

    [SyncVar]
    private int health = 3;
    private Text hp;
    
  
    // Onko kohde vihollinen (muokkauksen tarpeessa)
    public bool isEnemy = true;
    // Vahinko
    public void Dmg(int dmgCount)
    {
        health -= dmgCount;
        // Jos elämät on 0, tuhoa objekti
        if (health <= 0)
        {
            Destroy(gameObject);
        }
    }
    // Ammuksen osuessa/törmätessä, tee sille asetettu vahinko ja tuhoa ammus sen jälkeen
    void OnTriggerEnter2D(Collider2D otherCollider)
    {
        Shot shot = otherCollider.gameObject.GetComponent<Shot>();
        
        if (shot != null)
        {

            if (shot.isShot != isEnemy)
            {
                Dmg(shot.dmg);
                Destroy(shot.gameObject);
            }
        }
    }
}
