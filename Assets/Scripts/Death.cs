﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Death : NetworkBehaviour {

    private hp hpscript;

	// Use this for initialization
	void Start () {

        hpscript = GetComponent<hp>();

        hpscript.EventDie += CmdDisable;
	}

    void OnDisable()
    {
        hpscript.EventDie += CmdDisable;
        if (isLocalPlayer && hpscript.isDead == true)
        {
            //GUI.Label(new Rect(50, 50, 100, 25), "Ded");
            NetworkClient.ShutdownAll();
        }
        
    }

    [Command]void CmdDisable()
    {
        GetComponent<controller>().enabled = false;
        GetComponent<Shoot>().enabled = false;
        GetComponent<SpriteRenderer>().enabled = false;
        
        /*Renderer[] renderers = GetComponentsInChildren<Renderer>();
        foreach (Renderer ren in renderers)
         {
             ren.enabled = false;
         }*/

        
        hpscript.isDead = true;   
    }
}
