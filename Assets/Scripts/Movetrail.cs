﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Movetrail : NetworkBehaviour {

    public int bulletSpeed = 30;

    //public Rigidbody2D rb;

    //public float bSpeed = 20f;

    //public Transform direction = null;

    void Start() {

        //rb = GetComponent<Rigidbody2D>();
        Destroy(gameObject, 1);
    }

	void Update () {

        if (transform.eulerAngles.y == 0)
        {
            transform.Translate(-transform.right * Time.deltaTime * bulletSpeed);
        }
        else
        {
            transform.Translate(transform.right * Time.deltaTime * bulletSpeed);
        }
        //rb.AddForce(direction.right * bulletSpeed);
	}
}
