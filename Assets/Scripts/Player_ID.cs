﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Player_ID : NetworkBehaviour {

    [SyncVar]
    public string playerUnique;
    private NetworkInstanceId pNetID;
    private Transform ntransform;

    public override void OnStartLocalPlayer()
    {
        GetNetID();
        SetID();
    }

	// Use this for initialization
	void Awake () 
    {
        ntransform = transform;
	}
	
	// Update is called once per frame
	void Update () 
    {
	    if(ntransform.name == "" || ntransform.name == "Player(Clone)")
        {
            SetID();
        }
	}

    [Client]
    void GetNetID()
    {
        pNetID = GetComponent<NetworkIdentity>().netId;
        CmdTellServerMyID(MakeUniqueID());
    }
  
    void SetID()
    {
        if (!isLocalPlayer)
        {
            ntransform.name = playerUnique;
        }
        else
        {
            ntransform.name = MakeUniqueID();
        }
    }

    string MakeUniqueID()
    {
        string uniqueName = "Player" + pNetID.ToString();
        return uniqueName;
    }

    [Command]
    void CmdTellServerMyID(string name)
    {
        playerUnique = name;
    }
}
