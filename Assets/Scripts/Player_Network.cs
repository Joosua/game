﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;


public class Player_Network : NetworkBehaviour {

	void Start () {

        if (isLocalPlayer)
        {
            GetComponent<controller>().enabled = true;
            GetComponent<Shoot>().enabled = true;
            GetComponent<hp>().enabled = true;
            GetComponent<NetworkAnimator>().SetParameterAutoSend(0, true);
        }
	}

    public override void PreStartClient()
    {
        GetComponent<NetworkAnimator>().SetParameterAutoSend(0, true);
    }
}
