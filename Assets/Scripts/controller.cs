﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
using System.Collections;

public class controller : NetworkBehaviour
{

    #region Atribuutit
    // Hahmon nopeus
    public float speedF = 5f;

    [SyncVar(hook="UpdateRotate")]public Vector2 rotate;

    public Vector2 jumpVec;
    // Ground check bool
    public bool grounded;
    // Ympyrä, joka tarkistaa, onko pelaaja maassa
    public Transform grounder;
    // Ympyrän mitta
    public float radius;
    // Maan layeri
    public LayerMask ground;
    // Rigidbody
    Rigidbody2D rb;
    // Animaattori, jolla voidaan vaihtaa hahmon animaatiotilaa tietyissä kohdissa
    public Animator anim;
    
    #endregion
    
	void Start ()
    {
        #region Alustukset
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        #endregion
    }

    
    
	void Update ()
    {
        #region Liikkuminen
        // Liikkuminen + idle
        if (isLocalPlayer)
        {
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                rb.velocity = new Vector2(-speedF, rb.velocity.y);
                //transform.localScale = new Vector3(-1, 1, 1); // Suunta, johon hahmo katsoo
                //transform.eulerAngles = new Vector2(0,180);
                CmdFlip(false);
                anim.SetInteger("AnimationState", 1); // Unity-editorin puolella luotu Integer, jonka avulla voidaan hallita animaatiotiloja (1 on esimerkiksi juoksu, jne.)
            }
            else if (Input.GetKey(KeyCode.RightArrow))
            {
                rb.velocity = new Vector2(speedF, rb.velocity.y);
                //transform.localScale = new Vector3(1, 1, 1);
                //transform.eulerAngles = new Vector2(0,0);
                CmdFlip(true);
                anim.SetInteger("AnimationState", 1);
            }
            else
            {
                rb.velocity = new Vector2(0, rb.velocity.y);
                anim.SetInteger("AnimationState", 0);
            }

            // Hyppy
            grounded = Physics2D.OverlapCircle(grounder.transform.position, radius, ground); // Ympyrä, joka tarkistaa ground-tilan

            if (Input.GetKey(KeyCode.UpArrow) && grounded == true)
            {
                rb.AddForce(jumpVec, ForceMode2D.Force);
                anim.SetInteger("AnimationState", 2);
            }

            // Taunt
            if (Input.GetKey(KeyCode.DownArrow))
            {
                anim.SetInteger("AnimationState", 3);
            }

            // Reset
            if (Input.GetKey(KeyCode.Escape))
            {
                SceneManager.LoadScene("start");
                //NetworkManager.Shutdown();
                //Network.Disconnect();
                //MasterServer.UnregisterHost();
                
            }

        }
        #endregion

        #region Rajaus

        if (transform.position.x <= -3.9f)
        {
            transform.position = new Vector2(-3.9f, transform.position.y);
        }
        else if (transform.position.x >= 3.9f) 
        {
            transform.position = new Vector2(3.9f, transform.position.y);
        }

        if (transform.position.y <= -3.50f)
        {
            transform.position = new Vector2(transform.position.x, -3.50f);
        }
        else if (transform.position.y >= 3.50f)
        {
            transform.position = new Vector2(transform.position.x, 3.50f);
        }

        #endregion
    }
    #region Devhelp
    // Devhelp (Editoriin piirto (punainen groundcheck ympyrä)). Ei näy itse pelissä
    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(grounder.transform.position , radius);
    }
    #endregion

    [Command]
   public void CmdFlip(bool flip)
    {
            if (flip)
            {
                rotate = new Vector2(0, 180);
            }
            else
            {
                rotate = new Vector2(0, 0);
            } 
    }

    void UpdateRotate(Vector2 newRotate)
    {
        transform.eulerAngles = newRotate;
    }
}
