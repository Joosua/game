﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;

public class hp : NetworkBehaviour {

    [SyncVar (hook="OnChange")]
    private int health = 3;
    private Text hpText;
    private bool shouldDie = false;
    public bool isDead = false;

    public delegate void dieDelegate();
    public event dieDelegate EventDie;

    //public delegate void respawnDelegate();
    //public event respawnDelegate EventRespawn;

	void Start () {
        hpText = GameObject.Find("Health").GetComponent<Text>();
        SetHpText();
	}
	
	void Update () {
        checkCondition();
	}

    void SetHpText()
    {
        if (isLocalPlayer)
        {
            hpText.text = "Health" + health.ToString();
        }
    }

    public void lowerHealth(int dmg)
    {
        health -= dmg;
    }

    void OnChange(int hlth)
    {
        health = hlth;
        SetHpText();        
    }

    void checkCondition()
    {
        if (health <= 0 && !shouldDie && !isDead)
        {
            shouldDie = true;
        }

        if (health <= 0 && shouldDie)
        {
            if (EventDie != null)
            {
                EventDie();
            }
            shouldDie = false;
        }
    }
}
