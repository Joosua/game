﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Collections.Generic;

[NetworkSettings(channel = 0, sendInterval = 0.033f)]
public class Player_SyncPos : NetworkBehaviour {

    [SyncVar (hook = "SyncPosition")]
    private Vector2 syncPosition;

    [SerializeField]
    Transform mtransform;
    [SerializeField]
    float lerpRate;
    float normalLerp = 16;
    float fasterLerp = 24;

    private Vector2 lastPosition;
    private float threshold = 0.1f;

    private NetworkClient nworkClient;
    private int latency;
    private Text latencyTxt;

    private List<Vector2> syncPositionList = new List<Vector2>();
    [SerializeField]
    private bool usePastLerp = false;
    private float close = 0.2f;

    void Start()
    {
        nworkClient = GameObject.Find("NetworkManager").GetComponent<NetworkManager>().client;
        latencyTxt = GameObject.Find("Latency").GetComponent<Text>();
        lerpRate = normalLerp;
    }
	void FixedUpdate () 
    {
        TransmitPos();
        LerpPosition();
        Latency();
	}

    void LerpPosition()
    {
        if (!isLocalPlayer)
        {
            if (usePastLerp)
            {

            }
            else
            {
                NormalLerping();
            }
        }
    }

    [Command]
    void CmdProvidePosToServer(Vector2 pos)
    {
        syncPosition = pos;
    }

    [ClientCallback]
    void TransmitPos()
    {
        if (isLocalPlayer && Vector2.Distance(mtransform.position, lastPosition) > threshold)
        {
            CmdProvidePosToServer(mtransform.position);
            lastPosition = mtransform.position;
        }
    }

    void SyncPosition(Vector2 latestPos)
    {
        syncPosition = latestPos;
        syncPositionList.Add(syncPosition);
    }

    void Latency()
    {
        if (isLocalPlayer)
        {
            latency = nworkClient.GetRTT();
            latencyTxt.text = latency.ToString();
        }
    }

    void NormalLerping()
    {
        mtransform.position = Vector2.Lerp(mtransform.position, syncPosition, Time.deltaTime * lerpRate);
    }

    void PastLerping()
    {
        if (syncPositionList.Count > 0)
        {
            mtransform.position = Vector2.Lerp(mtransform.position, syncPositionList[0], Time.deltaTime * lerpRate);

            if (Vector2.Distance(mtransform.position , syncPositionList[0]) < close)
            {
                syncPositionList.RemoveAt(0);
            }

            if (syncPositionList.Count > 10)
            {
                lerpRate = fasterLerp;
            }
            else
            {
                lerpRate = normalLerp;
            }
        }
    }
}
