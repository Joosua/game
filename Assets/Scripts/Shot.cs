﻿using UnityEngine;
using System.Collections;

public class Shot : MonoBehaviour {
    
    // Ammuksen vahinko
    public int dmg = 1;

    public bool isShot = false;
    
    // Tuhoaa ammuksen 20 sekunnin jälkeen, vaikka se ei osuisi (ei kuormita liikaa)
	void Start () {

        if (gameObject.name == "Fireballt(Clone)")
        {
        Destroy(gameObject, 20);
        }
	}
	
}
