﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class blink : MonoBehaviour {

    public GameObject start;

    public float interval;

	// Use this for initialization
	void Start () {
        InvokeRepeating("Blink",0,interval);
	}
	
	// Update is called once per frame
	void Update () {
	
        if(Input.GetKey(KeyCode.Return))
        {
            //Application.LoadLevel("gamescene");
            SceneManager.LoadScene("gamescene");
        }

        if(Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }

	}

    void Blink()
    {
        if (start.activeSelf)
        {
            start.SetActive(false);
        }
        else
        {
            start.SetActive(true);
        }
    }
}
